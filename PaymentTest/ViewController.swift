//
//  ViewController.swift
//  PaymentTest
//
//  Created by M R on 7/15/17.
//  Copyright © 2017 M R. All rights reserved.
//

import UIKit


class ViewController: UIViewController, FaturahTransactionManagerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func BuyButtonClicked(_ sender: Any) {
        let transaction = FaturahTransaction()
        transaction.merchantCode = "0c64b7c951c32ba2449ef04382ae00ef"
        transaction.secureKey = "f3569a83-54f4-4d98-abaa-891b285c6e09"
        transaction.urlScheme = "faturah123"
        
        let order = FaturahOrder()
        let orderItem = FaturahOrderItem()
        orderItem.itemID = "3"
        orderItem.itemName = "iPhone 6"
        orderItem.itemPrice = 250
        orderItem.itemQuantity = 2
        orderItem.itemDescription = "iPhone 6 and its accessories"
        
        order.add(orderItem)
        
        order.orderDeliveryCharge = 5
        order.orderCustomerName = "Mohamed Ramadan"
        order.orderCustomerEmail = "email@website.com"
        order.orderCustomerPhone = "0123456789"
        order.orderLanguage = "ar"
        
        transaction.order = order
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        FaturahTransactionManager.shared().prepareTranscation(transaction, with: self)
    }
    
    // MARK:- FaturahTransactionManagerDelegate
    
    func transactionManagerDidFinishTransactionPreparation(_ transaction: FaturahTransaction!, withError error: Error!) {
        MBProgressHUD.hide(for: self.view, animated: true)
        
        if((error) != nil){
            print("Error Occured During Transaction Preparation")
        }else{
            FaturahTransactionManager.shared().startPayement(for: transaction, from: self, with: self)
        }
    }
    
    func transactionManagerDidFinishPayment(with status: FaturahPaymentStatus, andError error: Error!) {
        var message = ""
        switch status {
        case .completed:
            message = "Payment Completed"
            break
        case .failed:
            message = "Payment Failed"
            break
        case .pending:
            message = "Payment Pending"
            break
        case .canceled:
            message = "Payment Canceled"
            break
        default:
            break
        }
        
        let alert:UIAlertController = UIAlertController.init(title: "", message: message, preferredStyle: .alert)
        let action:UIAlertAction = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
    }
    
    
}

